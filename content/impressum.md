+++
title = "Impressum"
formspree = true
+++

## Angaben gemäß § 5 TMG und Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:
Matthias Kerk<br>
Kremmener Str. 43<br>
16515 Oranienburg

## Kontakt:
 |
---|---
__Telefon:__ | +49 3301 835784
__Telefax:__ | +49 3301 835783
__E-Mail:__ | matthias@tuxlife.net

## Umsatzsteuer-ID:

Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:
**DE305816923**

* * *

# Haftungsausschluss (Disclaimer)

**Haftung für Inhalte**

Als Diensteanbieter bin ich gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen
Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG bin
ich als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder
gespeicherte fremde Informationen zu überwachen oder nach Umständen zu
forschen, die auf eine rechtswidrige Tätigkeit hinweisen.

Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen
nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche
Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten
Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden
Rechtsverletzungen werden ich diese Inhalte umgehend entfernen.

**Haftung für Links**

Mein Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte
ich keinen Einfluss haben. Deshalb kann ich für diese fremden Inhalte auch
keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der
jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten
Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße
überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht
erkennbar.

Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne
konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei
Bekanntwerden von Rechtsverletzungen werde ich derartige Links umgehend
entfernen.

**Urheberrecht**

Der durch mir erstellten Inhalte und Werke auf diesen Seiten unterliegen dem
deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und
jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der
schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und
Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch
gestattet.

Soweit die Inhalte auf dieser Seite nicht von mir erstellt wurden,werden die
Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche
gekennzeichnet. Solltest Du trotzdem auf eine Urheberrechtsverletzung
aufmerksam werden, bitten ich um einen entsprechenden Hinweis. Bei
Bekanntwerden von Rechtsverletzungen werden ich derartige Inhalte umgehend
entfernen.

# Datenschutzerklärung

**Datenschutz**

Ich nehme den Schutz Deiner persönlichen Daten sehr ernst. Ich behandeln Deine
personenbezogenen Daten vertraulich und entsprechend der gesetzlichen
Datenschutzvorschriften sowie dieser Datenschutzerklärung.

Die Nutzung meiner Webseite ist in der Regel ohne Angabe personenbezogener
Daten möglich. Soweit auf meiner Seiten personenbezogene Daten
(beispielsweise Name, Anschrift oder E-Mail-Adressen) erhoben werden, erfolgt
dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne
Deine ausdrückliche Zustimmung nicht an Dritte weitergegeben.

Ich weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der
Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser
Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.

**Datenschutzerklärung für die Nutzung von Facebook-Plugins (Like-Button)**

Auf meiner Seiten sind Plugins des sozialen Netzwerks Facebook, Anbieter
Facebook Inc., 1 Hacker Way, Menlo Park, California 94025, USA, integriert.
Die Facebook-Plugins erkennst Du an dem Facebook-Logo oder dem "Like-Button"
("Gefällt mir") auf meiner Seite. Eine übersicht über die Facebook-Plugins
findest Du hier: <http://developers.facebook.com/docs/plugins/>.

Wenn Du meine Seiten besuchst, wird über das Plugin eine direkte Verbindung
zwischen Deinem Browser und dem Facebook-Server hergestellt. Facebook erhält
dadurch die Information, dass Du mit Deiner IP-Adresse meine Seite besucht
habst. Wenn Du den Facebook "Like-Button" anklickst während Du in Deinem
Facebook-Account eingeloggt bist, kannst Du die Inhalte meiner Seiten auf
Deinem Facebook-Profil verlinken. Dadurch kann Facebook den Besuch meiner
Seiten Deinem Benutzerkonto zuordnen. Ich weisen darauf hin, dass ich als
Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie
deren Nutzung durch Facebook erhalte. Weitere Informationen hierzu findst Du
in der Datenschutzerklärung von Facebook unter
<http://de-de.facebook.com/policy.php>.

Wenn Du nicht wünschst, dass Facebook den Besuch unserer Seiten Ihrem
Facebook-Nutzerkonto zuordnen kann, logge Dich bitte aus Deinem Facebook-
Benutzerkonto aus.

**Datenschutzerklärung für die Nutzung von Google Analytics**

Diese Website nutzt Funktionen des Webanalysedienstes Google Analytics.
Anbieter ist die Google Inc., 1600 Amphitheatre Parkway Mountain View, CA
94043, USA.

Google Analytics verwendet sog. "Cookies". Das sind Textdateien, die auf Deinem
Computer gespeichert werden und die eine Analyse der Benutzung der Website
durch Dich ermöglicht. Die durch den Cookie erzeugten Informationen über Deine
Benutzung dieser Website werden in der Regel an einen Server von Google in den
USA übertragen und dort gespeichert.

Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre
IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen
Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen
Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse
an einen Server von Google in den USA übertragen und dort gekürzt. In meinem
Auftrag wird Google diese Informationen benutzen, um Deine Nutzung der Website
auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um
weitere mit der Websitenutzung und der Internetnutzung verbundene
Dienstleistungen gegenüber mir zu erbringen. Die im Rahmen von Google Analytics
von Deinem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von
Google zusammengeführt.

Du kannst die Speicherung der Cookies durch eine entsprechende Einstellung
Deiner Browser-Software verhindern; ich weisen Dich jedoch darauf hin, dass Du
in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website
vollumfänglich nutzen werden kannst. Du kannst darüber hinaus die Erfassung
der durch das Cookie erzeugten und auf Deiner Nutzung der Website bezogenen
Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten
durch Google verhindern, indem Du das unter dem folgenden Link verfügbare
Browser-Plugin herunterladen und installieren:
<http://tools.google.com/dlpage/gaoptout?hl=de>

Du kannst die Erfassung durch Google Analytics verhindern, indem Du auf
folgenden Link klickst. Es wird ein Opt-Out-Cookie gesetzt, das das Erfassung
Deiner Daten bei zukünftigen Besuchen dieser Website verhindert: [Google
Analytics deaktivieren](javascript:gaOptout\(\))

**Datenschutzerklärung für die Nutzung von Google +1**

Meine Seiten nutzen Funktionen von Google +1. Anbieter ist die Google Inc.,
1600 Amphitheatre Parkway Mountain View, CA 94043, USA.

Erfassung und Weitergabe von Informationen: Mithilfe der Google
+1-Schaltfläche kannst Du Informationen weltweit veröffentlichen. Über die
Google +1-Schaltfläche erhaltst Du und andere Nutzer personalisierte Inhalte
von Google und unseren Partnern. Google speichert sowohl die Information, dass
Du für einen Inhalt +1 gegeben hast, als auch Informationen über die Seite,
die Du beim Klicken auf +1 angesehen hast. Dein +1 können als Hinweise
zusammen mit Deinem Profilnamen und Deinem Foto in Google-Diensten, wie etwa in
Suchergebnissen oder in Deinem Google-Profil, oder an anderen Stellen auf
Websites und Anzeigen im Internet eingeblendet werden.

Google zeichnet Informationen über Deine +1-Aktivitäten auf, um die Google-
Dienste für Dich und andere zu verbessern. Um die Google +1-Schaltfläche
verwenden zu können, benötigst Du ein weltweit sichtbares, öffentliches
Google-Profil, das zumindest den für das Profil gewählten Namen enthalten
muss. Dieser Name wird in allen Google-Diensten verwendet. In manchen Fällen
kann dieser Name auch einen anderen Namen ersetzen, den Du beim Teilen von
Inhalten über Dein Google-Konto verwendet hast. Die Identität Deines Google-
Profils kann Nutzern angezeigt werden, die Deine E-Mail-Adresse kennen oder
über andere identifizierende Informationen von Dir verfügen.

Verwendung der erfassten Informationen: Neben den oben erläuterten
Verwendungszwecken werden die von Deinen bereitgestellten Informationen gemäß
den geltenden Google-Datenschutzbestimmungen genutzt. Google veröffentlicht
möglicherweise zusammengefasste Statistiken über die +1-Aktivitäten der Nutzer
bzw. gibt diese an Nutzer und Partner weiter, wie etwa Publisher, Inserenten
oder verbundene Websites.

**Datenschutzerklärung für die Nutzung von Instagram**

Auf meiner Seiten sind Funktionen des Dienstes Instagram eingebunden. Diese
Funktionen werden angeboten durch die Instagram Inc., 1601 Willow Road, Menlo
Park, CA, 94025, USA integriert. Wenn Du in deinem Instagram - Account
eingeloggt bist, kannst Du durch Anklicken des Instagram - Buttons die Inhalte
meiner Seiten mit Deinem Instagram - Profil verlinken. Dadurch kann Instagram
den Besuch meiner Seiten Deinem Benutzerkonto zuordnen. Ich weisen darauf hin,
dass ich als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten
Daten sowie deren Nutzung durch Instagram erhalten.

Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von
Instagram: <http://instagram.com/about/legal/privacy/>

**Datenschutzerklärung für die Nutzung von LinkedIn**

Meine Website nutzt Funktionen des Netzwerks LinkedIn. Anbieter ist die
LinkedIn Corporation, 2029 Stierlin Court, Mountain View, CA 94043, USA. Bei
jedem Abruf einer meiner Seiten, die Funktionen von LinkedIn enthält, wird
eine Verbindung zu Servern von LinkedIn aufbaut. LinkedIn wird darüber
informiert, dass Du unsere Internetseiten mit Deiner IP-Adresse besucht hast.
Wenn Du den "Recommend-Button" von LinkedIn anklickst und in Deinem Account
bei LinkedIn eingeloggt bist, ist es LinkedIn möglich, Deinen Besuch auf
meiner Internetseite Dir und Deinem Benutzerkonto zuzuordnen. Ich weisen
darauf hin, dass ich als Anbieter der Seiten keine Kenntnis vom Inhalt der
übermittelten Daten sowie deren Nutzung durch LinkedIn haben.

Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von
LinkedIn unter: <https://www.linkedin.com/legal/privacy-policy>

**Datenschutzerklärung für die Nutzung von Twitter**

Auf meinen Seiten sind Funktionen des Dienstes Twitter eingebunden. Diese
Funktionen werden angeboten durch die Twitter Inc., 1355 Market Street, Suite
900, San Francisco, CA 94103, USA. Durch das Benutzen von Twitter und der
Funktion "Re-Tweet" werden die von Dir besuchten Webseiten mit Deinem
Twitter-Account verknüpft und anderen Nutzern bekannt gegeben. Dabei werden
auch Daten an Twitter übertragen. Ich weisen darauf hin, dass ich als Anbieter
der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren
Nutzung durch Twitter erhalten. Weitere Informationen hierzu finden Sie in der
Datenschutzerklärung von Twitter unter <http://twitter.com/privacy>.

Deine Datenschutzeinstellungen bei Twitter kannst Du in den Konto-
Einstellungen unter <http://twitter.com/account/settings> ändern.

**Datenschutzerklärung für die Nutzung von Xing**

Meine Webseite nutzt Funktionen des Netzwerks XING. Anbieter ist die XING AG,
Dammtorstraße 29-32, 20354 Hamburg, Deutschland. Bei jedem Abruf einer meiner
Seiten, die Funktionen von Xing enthält, wird eine Verbindung zu Servern von
Xing hergestellt. Eine Speicherung von personenbezogenen Daten erfolgt dabei
nach meiner Kenntnis nicht. Insbesondere werden keine IP-Adressen gespeichert
oder das Nutzungsverhalten ausgewertet.

Weitere Information zum Datenschutz und dem Xing Share-Button finden Sie in
der Datenschutzerklärung von Xing unter
<https://www.xing.com/app/share?op=data_protection>

**Datenschutzerklärung für die Nutzung von YouTube**

Meine Webseite nutzt Plugins der von Google betriebenen Seite YouTube.
Betreiber der Seiten ist die YouTube, LLC, 901 Cherry Ave., San Bruno, CA
94066, USA. Wenn Du eine meiner mit einem YouTube-Plugin ausgestatteten
Seiten besuchst, wird eine Verbindung zu den Servern von YouTube hergestellt.
Dabei wird dem Youtube-Server mitgeteilt, welche meiner Seiten Du besucht
hast.

Wenn Du in deinem YouTube-Account eingeloggt bist, ermöglichst Du YouTube,
Dein Surfverhalten direkt Deinem persönlichen Profil zuzuordnen. Dies kannst Du
verhindern, indem Du dich aus Deinem YouTube-Account ausloggst.

Weitere Informationen zum Umgang von Nutzerdaten finden Sie in der
Datenschutzerklärung von YouTube unter
<https://www.google.de/intl/de/policies/privacy>

**Cookies**

Die Internetseiten verwenden teilweise so genannte Cookies. Cookies richten
auf Deinem Rechner keinen Schaden an und enthalten keine Viren. Cookies dienen
dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen.
Cookies sind kleine Textdateien, die auf Deinem Rechner abgelegt werden und die
Dein Browser speichert.

Die meisten der von mir verwendeten Cookies sind so genannte „Session-
Cookies“. Sie werden nach Ende Deines Besuchs automatisch gelöscht. Andere
Cookies bleiben auf Dein Endgerät gespeichert, bis Du diese löschst. Diese
Cookies ermöglichen es mir, Dein Browser beim nächsten Besuch wiederzuerkennen.

Du kannst Deinem Browser so einstellen, dass Du über das Setzen von Cookies
informiert wirst und Cookies nur im Einzelfall erlauben, die Annahme von
Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische
Löschen der Cookies beim Schließen des Browser aktivieren. Bei der
Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt
sein.

**Kontaktformular**

Wenn Du mir per Kontaktformular Anfragen zukommen lässt, werden Deine Angaben
aus dem Anfrageformular inklusive der von Dir dort angegebenen Kontaktdaten
zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen bei mir
gespeichert. Diese Daten geben ich nicht ohne Deine Einwilligung weiter.

Original-Quelle: _<https://www.e-recht24.de>_
