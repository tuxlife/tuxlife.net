+++
title = "Kontakt"
formspree = true
+++

Beschreib bitte kurz Dein Anliegen und wie ich Dir/Euch dabei unterstützen kann. Ich melde mich dann so bald wie möglich zurück!

Bitte beachtet, dass sich mein Angebot ausschließlich an Firmen, Gewerbetreibende, Freiberufler, Vereine und andere nicht private Einrichtungen richtet.

Wie es so schön heißt: _Fragen kostet nichts_